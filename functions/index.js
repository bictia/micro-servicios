const functions = require('firebase-functions');
const admin = require('firebase-admin');
//const email = require('nodemailer');
admin.initializeApp(functions.config().firebase);

const fb = admin.firestore();
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

exports.addUser = functions.auth.user().onCreate((user) => {
    fb.collection("users").doc(user.uid).set({
        id: null,
        age: null,
        childsNumber: null,
        civilState: null,
        info: null,
        device: null
    });
});

exports.deleteUser = functions.auth.user().onDelete((user) => {
    fb.collection("users").doc(user.uid).delete();
});